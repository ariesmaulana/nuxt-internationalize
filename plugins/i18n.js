import Vue from "vue"
import VueI18n from "vue-i18n"
Vue.use(VueI18n)

export default ({ app, store }) => {
    // di sini kita akan memanfaat kan store (vuex)
    app.i18n = new VueI18n({
    // `locale` akan mengambil dari locale default di store(vuex)
    locale: store.state.locale,
    fallbackLocale: "id", //selalu gunakan ID jika kaga ada Inggris
    messages: {
      //lokasi file terjemahan
      en: require("~/locales/en.json"),
      id: require("~/locales/id.json")
    }
  })
}
