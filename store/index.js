export const state = () => ({
  locales: ["id", "en"],
  locale: "id"
});

export const mutations = {
  SET_LANG(state, locale) {
    if (state.locales.indexOf(locale) !== -1) {
      state.locale = locale;
    }
  }
};
