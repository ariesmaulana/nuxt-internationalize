export default function({ isHMR, app, store, route, params, error, redirect }) {
  // ambil  
  const defaultLocale = app.i18n.fallbackLocale
  // jika halaman bersumber dari url langsung atau hot reload abaikan saja
  if (isHMR) return
  // jika tidak ada query string 'lang' ambil defaultlocale yaitu id
  let locale = route.query.lang || defaultLocale
  // jika tidak ditemukan di state vuex paksa jadi id
  if (store.state.locales.indexOf(locale) === -1) {
    locale = defaultLocale
  }
  //ubah state locale jadi locale yang dipilih
  store.commit("SET_LANG", locale)
  // Set locale dari query string '?lang='**''
  app.i18n.locale = store.state.locale
}
